const buscarPeli = () => {
    const nomPeli = document.getElementById('nomPeli').value;
    const url = "https://www.omdbapi.com/?t="+nomPeli+"&plot=full&apikey=30063268";
    fetch(url)
        .then(respuesta => respuesta.json())  
        .then(data => mosRes(data))
        .catch(reject => { 
            const contenedor = document.getElementById('contenido');
            contenedor.innerHTML = "Surgió un error " +reject;
        });
};

// MOSTRAMOS LOS RESULTADOS
const mosRes = (data) => {
    const contenedor = document.getElementById('contenido');

    // Limpiamos el contenido anterior en cada búsqueda
    contenedor.innerHTML = "";

    // Verificamos si la respuesta tiene errores
    if (data.Error) {
        contenedor.innerHTML = "Error: No se encontro la pelicula con ese nombre";
    } else {
        contenedor.innerHTML += '<label for="nomPeli" id="nomPeli"><b>Nombre:</b> '+data.Title+'</label>' +
            '<label for="anioReal" id="anioReal"><b>Año de Realización:</b> '+data.Released+'</label>'+
            '<img src="'+data.Poster+'" alt="imgPeli">'+' <label for="resenia"><b>Reseña</b></label> <textarea id="resenia"></textarea>'+
            '<label for="actoresPrin"><b>Actores Principales:</b> '+data.Actors+'</label>';

        document.getElementById('resenia').value = data.Plot;
    }
}


// CODIFICAMOS EL BOTON
document.getElementById('btnBuscar').addEventListener('click', function () {
    buscarPeli();
});
